## Export plays from BGA

Procedure is described in first cells of BGA-export.ods, that is:
1. Go to your BGA gamestats page : https://boardgamearena.com/player?section=lastresults then "\[usename\]'s games history" in table head (boardgamearena.com/gamestats?player=xxxx)
2. Click one on "See more" at the bottom of the table, then hold return key to load the rest
3. Run thoses line in javascript console (usually press F12 to open it), this will help parsing data:
```
names = document.getElementsByClassName('playername')
for (let item of names) {
item.innerHTML = '# '+item.innerHTML+' #';
}
ranks = document.getElementsByClassName('rank')
for (let item of ranks) {
item.innerHTML = '! '+item.innerHTML;
}
scores = document.getElementsByClassName('score')
for (let item of scores) {
item.innerHTML += ' ?';
}
times = document.getElementsByClassName('smalltext')
for (let item of times) {
item.innerHTML += ' ';
}
```

4. Select the 3 first columns (we're not interested by ELO variations): on modern browsers, hold Ctrl key then click-and-drag from one cell to another, then copy it (ctrl+c)
5. Paste them in BGA-export.ods, from column A (erase pre-filled example).
6. Select the A column, then press backspace key, select only "Objects" in pop up and confirm (this will remove games logos)
7. Copy/drag D:F till the bottom of your data
8. And that's it! Export columns C:F as CSV (you may copy them to another sheet, then File > Save a copy)

## Import to BGG

You need to set several information before running script.

1. Copy `conf.dist.php` to `conf.local.php`, then open it to edit cookie line
2. Get your cookie header string by opening your web browser console (usually F12), go to the network tab, load a BGG page (when logged in), select the first request, and find it in "Header request"
3. Craft you games table (you can get list on your BGA gamestat, "Most played games") by associating their BGG objectid (found in URL, like 264055 in https://boardgamegeek.com/boardgame/264055/draftosaurus), in `BGG-import.php` in `$games` array
4. You may also replace `$myname` and `$myusername` with your BGA name et BGG username :)

I think that's it! Then launch script with `php BGG-import.php`!
There's also a script (`plays_id.php`) that may help you delete plays if you mess up with your import.
